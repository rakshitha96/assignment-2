provider "aws" {
 region = "us-west-1"
}

resource "aws_instance" "ubuntu" {
 ami = var.amiid
 instance_type = var.type
 key_name = var.pemfile
 user_data = <<EOF
   #! /bin/bash
   sudo apt-get update
   sudo apt install python3
 EOF

 provisioner "local-exec" {
    command = "echo ${aws_instance.ubuntu.private_ip}" 
  }
}
