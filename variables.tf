variable "amiid" {
 default = "ami-0d382e80be7ffdae5"
}

variable "type" {
 default = "t2.micro"
}

variable "pemfile" {
 default = "new_pem"
}
